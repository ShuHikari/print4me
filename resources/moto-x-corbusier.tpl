<style>
div.design-template { border: 1px solid none; width: 338px; height: 560px; position: relative; opacity: 1; z-index: 50;
	-moz-transition-property: opacity, border; -moz-transition-duration: 0.5s;
	-webkit-transition-property: opacity, border; -webkit-transition-duration: 0.5s;
	transition-property: opacity, border; transition-duration: 0.5s; }
div.design-template > div.placeholder { position: absolute; background-color: rgba(255,255,255,0.001);; border: 1px solid rgba(30,30,30,0.05);
	-moz-box-sizing: border-box; -webkit-box-sizing: border-box; -ms-box-sizing: border-box; box-sizing: border-box; 
	-moz-transition-property: opacity, border, background; -moz-transition-duration: 0.5s;
	-webkit-transition-property: opacity, border, background; -webkit-transition-duration: 0.5s;
	transition-property: opacity, border, background; transition-duration: 0.5s;
	margin-left: -1px; margin-top: -1px }

div.design-template .small-grid { width: 62px; height: 62px; } /* Originally it should be 60px, but 2px in addition to cater for overlapping borders. */
div.design-template .big-grid { width: 128px; height: 128px; } /* Originally it should be 126px, but 2px in addition to cater for overlapping borders. */

div.design-template.over > div.placeholder { border-width: 1px; border-style: solid; border-color: #c1beb9;
   -moz-box-sizing: border-box; -webkit-box-sizing: border-box; -ms-box-sizing: border-box; box-sizing: border-box; }
div.design-template > div.placeholder.over { background-color: #cccccc; opacity: 0.5 }
div.design-template img.img-holder { opacity: 0; display: block;
	-moz-transition-property: opacity, border, background, box-shadow; -moz-transition-duration: 0.3s;
	-webkit-transition-property: opacity, border, background, box-shadow; -webkit-transition-duration: 0.3s;
	-ms-transition-property: opacity, border, background, box-shadow; -ms-transition-duration: 0.3s;
	transition-property: opacity, border, background, box-shadow; transition-duration: 0.3s;
}
div.design-template div.img-holder { opacity: 0; display: block;
        -moz-transition-property: opacity, border, background, box-shadow; -moz-transition-duration: 0.3s;
        -webkit-transition-property: opacity, border, background, box-shadow; -webkit-transition-duration: 0.3s;
        -ms-transition-property: opacity, border, background, box-shadow; -ms-transition-duration: 0.3s;
        transition-property: opacity, border, background, box-shadow; transition-duration: 0.3s;
}
div.design-template div.img-holder img{ filter:alpha(opacity=0) }

div.design-template > div.placeholder:hover { z-index: 50; }
/*div.design-template > div.placeholder:hover img.img-holder { 
	opacity: 1!important; display: block!important;
	-moz-transform: scale(1.2); -webkit-transform: scale(1.2); -ms-transform: scale(1.2); transform: scale(1.2);
	-moz-box-shadow: 0 0 2px #c68a59; -webkit-box-shadow: 0 0 2px #c68a59; -ms-box-shadow: 0 0 2px #c68a59; box-shadow: 0 0 2px #c68a59; 
}*/
</style>
<div class="design-template" data-tpl="moto-x-corbusier">
	<!-- Grid = 60x60, Edge = 6 -->
  <!-- Horizontal: 20	86	152	218 -->
  <!-- Vertical: 18	84	150	216	282	348	414	480 -->
  
<div class="placeholder grabbable" data-tpid="0" style="top:6px; left:28px; width:141px; height:137px;"></div>
<div class="placeholder grabbable" data-tpid="1" style="top:6px; left:169px; width:71px; height:68px;"></div>
<div class="placeholder grabbable" data-tpid="2" style="top:6px; left:240px; width:70px; height:68px;"></div>
<div class="placeholder grabbable" data-tpid="3" style="top:74px; left:169px; width:71px; height:69px;"></div>
<div class="placeholder grabbable" data-tpid="4" style="top:74px; left:240px; width:70px; height:69px;"></div>
<div class="placeholder grabbable" data-tpid="5" style="top:143px; left:28px; width:70px; height:68px;"></div>
<div class="placeholder grabbable" data-tpid="6" style="top:143px; left:98px; width:71px; height:68px;"></div>
<div class="placeholder grabbable" data-tpid="7" style="top:211px; left:28px; width:70px; height:69px;"></div>
<div class="placeholder grabbable" data-tpid="8" style="top:211px; left:98px; width:71px; height:69px;"></div>
<div class="placeholder grabbable" data-tpid="9" style="top:143px; left:169px; width:141px; height:137px;"></div>
<div class="placeholder grabbable" data-tpid="10" style="top:280px; left:28px; width:141px; height:137px;"></div>
<div class="placeholder grabbable" data-tpid="11" style="top:280px; left:169px; width:71px; height:69px;"></div>
<div class="placeholder grabbable" data-tpid="12" style="top:280px; left:240px; width:70px; height:69px;"></div>
<div class="placeholder grabbable" data-tpid="13" style="top:349px; left:169px; width:71px; height:68px;"></div>
<div class="placeholder grabbable" data-tpid="14" style="top:349px; left:240px; width:70px; height:68px;"></div>
<div class="placeholder grabbable" data-tpid="15" style="top:417px; left:28px; width:70px; height:69px;"></div>
<div class="placeholder grabbable" data-tpid="16" style="top:417px; left:98px; width:71px; height:69px;"></div>
<div class="placeholder grabbable" data-tpid="17" style="top:486px; left:28px; width:70px; height:68px;"></div>
<div class="placeholder grabbable" data-tpid="18" style="top:486px; left:98px; width:71px; height:68px;"></div>
<div class="placeholder grabbable" data-tpid="19" style="top:417px; left:169px; width:141px; height:137px;"></div>


		
</div>

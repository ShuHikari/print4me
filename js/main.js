    'use strict';
    var fileInput = document.getElementById('imageInputs'),
        fileDisplayArea = document.getElementById('previewArea'),
        layout_btns = document.querySelectorAll('#grids .btn'),
        startPos = null;

    fileInput.addEventListener('change', function (e) {
        for (var i = 0; i < fileInput.files.length; i++) {
            var file = fileInput.files[i],
                imageType = /image.*/;

            if (file.type.match(imageType)) {
                var reader = new FileReader();

                reader.onload = function (e) {

                    var img = new Image(),
                        clear_btn = document.createElement('i'),
                        wrapper = document.createElement('div');
                    wrapper.className = "input-block-level";
                    clear_btn.className = "clear_btn";
                    clear_btn.innerHTML = 'remover';
                    $(clear_btn).on('click', function () {
                        $(this).parent().remove();
                    });
                    img.src = e.target.result;
                    img.className = "input-block-level draggable";
                    wrapper.appendChild(img);
                    wrapper.appendChild(clear_btn);
                    fileDisplayArea.appendChild(wrapper);
                }

                reader.readAsDataURL(file);
            } else {
                fileDisplayArea.innerHTML = "Arquivo não suportado"
            }
        }
    });

    for (var i = 0; i < layout_btns.length; i++) {
        layout_btns[i].addEventListener('click', function (e) {
            templateChange(e.target.getAttribute('data-tpl'));
            //        document.querySelector('#grids .btn').getAttribute('data-tp l')

        })
    };

    //TODO:Implementing drag listeners
    fileDisplayArea.draggable = true;

    //TODO: Set dropzone parameters based on image
    //document.getElementsByClassName('dropzone')[0].clientHeight
    //TODO: Snap to grid to make thumbs fit into order
    // target elements with the "draggable" class
    interact('.draggable')
        .draggable({
            restrict: {
                endOnly: true,
                drag: "",
                elementRect: {
                    top: 0,
                    left: 0,
                    bottom: 1,
                    right: 1
                }
            },
            onmove: dragMoveListener,
            onend: function (event) {
                if (classie.has(event.target, 'drag-element--dropped')) {
                    revertDraggable(event.target);
                }
            },
            inertia: true,
            autoScroll: true,
            snap: {
                mode: 'anchor',
                anchors: [],
                range: Infinity,
                relativePoints: {
                    x: 0.5,
                    y: 0.5
                },
                endOnly: true
            }
        })
        .on('dragstart', function (event) {
            startPos = {
                    x: event.x0,
                    y: event.y0
                }
                //Snap to initial position
            event.interactable.draggable({
                snap: {
                    targets: [startPos]
                }
            })
        });


    // enable draggables to be dropped into this
    interact('.dropzone')
        .dropzone({
            overlap: 'center',
            accept: '.draggable'
        })
        .on('dragenter', function (event) {
            var dropRect = interact.getElementRect(event.target),
                dropCenter = {
                    x: dropRect.left + dropRect.width / 2,
                    y: dropRect.top + dropRect.height / 2
                };
            event.draggable.snap({
                anchors: [dropCenter]
            });

            var draggableElement = event.relatedTarget,
                dropzoneElement = event.target;

            //drop 'feedback'
            dropzoneElement.classList.add('drop-target');
            draggableElement.classList.add('can-drop');
        })
        //whe the draggable leaves the dropzone
        .on('dragleave', function (event) {
            event.draggable.snap(false);
            //when leave the dropzone, get back to the start
            event.draggable.snap({
                anchors: [startPos]
            });
            //remove feedback style 
            event.target.classList.remove('drop-target');
            event.relatedTarget.classList.remove('can-drop');
        })
        .on('dropactivate', function (event) {
            //remove active dropzone feedback
            console.log(event.type);
            event.target.classList.remove('drop-target');
            event.target.classList.remove('drop-active');
        })
        .on('drop', function (event) {
            //verify if the target already have a canvas inside
            while (event.target.hasChildNodes()) {
                event.target.removeChild(event.target.childNodes[0]);
            }
            // Create the canvas to mask the image exibition
            //            var canvas = document.createElement('canvas'),
            //                ctx = canvas.getContext('2d');
            //            canvas.width = event.target.clientWidth;
            //            canvas.height = event.target.clientHeight;
            //            ctx.drawImage(event.relatedTarget, 0, 0);
            //            event.target.appendChild(canvas);
            //            canvas.addEventListener('dblclick', editMode)
            classie.add(event.relatedTarget, 'drag-element--dropped');
            var clone = $(event.relatedTarget).clone()[0];
            classie.removeClass(clone, 'draggable'); //TODO: remove draggable and create a function to disable
            classie.addClass(clone, 'drag-element--dropped');
            revertDraggable(clone);
            $(clone).attr('id', 'clone');
            event.target.style.overflow = 'hidden   ';
            clone.style.transform = '';
            $(clone).width('500px');
            $(clone).height('auto');
            $(clone).css({
                'margin-left': '-50%',
                'margin-top': '-50%'
            })
            $(event.target).append(clone);
            $(clone).on('dblclick', {
                square: event.target
            }, editMode)

        })
        .on('dropmove', function (event) {
            //            console.log(event.type)
        });

    // Dropzone Doubleclick Listener
    interact('.editable')
        .on('doubletap', function (event) {
            console.log(event.type)
        });



    // Specific and reusable functions

    function dragMoveListener(event) {
        var target = event.target,
            // keep the dragged position in the data-x/data-y attributes
            x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
            y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

        target.style.zIndex = 1000;
        target.style.opacity = 0.75;

        // translate the element
        target.style.webkitTransform =
            target.style.transform =
            'translate(' + x + 'px, ' + y + 'px)';

        // update the posiion attributes
        target.setAttribute('data-x', x);
        target.setAttribute('data-y', y);
    }

    function revertDraggable(el) {
        console.log('revert Draggable')
        el.style.opacity = 100;
        el.style.transform = el.style.webkitTransform = 'none';
        el.style.zIndex = 1;
        el.setAttribute('data-x', 0);
        el.setAttribute('data-y', 0);
    }

    function editMode(event) {
        var popup = $('#crop-popup'),
            parent = $(event.target.parentElement),
            button = $(document.createElement('button')),
            square = event.data.square;
        popup.width(parent.width());
        popup.height(parent.height());
        popup.css({
            'z-Index': 9999,
            'position': 'absolute'
        });
        popup.offset(parent.offset());
        popup.append(event.target);
        button.addClass('btn btn-raised');
        button.css({
            'z-Index': 9999,
            'position': 'absolute'
        });
        button.html('Ok');
        button.bind('click', function (square) {
            cropCanvas(square);
        });
        popup.after(button);

        //move try
        var v = $(popup);
        var c = $('#clone');

        interact(c[0]).draggable({
            inertia: true,
        }).resizable({
            preserveAspectRatio: true,
            edges: {
                left: true,
                right: true,
                bottom: true,
                top: true
            }
        }).on('resizemove', function (event) {
            var target = event.target,
                x = (parseFloat(target.getAttribute('data-x')) || 0),
                y = (parseFloat(target.getAttribute('data-y')) || 0);

            // update the element's style
            target.style.width = event.rect.width + 'px';
            target.style.height = event.rect.height + 'px';

            // translate when resizing from top or left edges
            x += event.deltaRect.left;
            y += event.deltaRect.top;

            target.style.webkitTransform = target.style.transform =
                'translate(' + x + 'px,' + y + 'px)';

            target.setAttribute('data-x', x);
            target.setAttribute('data-y', y);
            target.textContent = Math.round(event.rect.width) + '×' + Math.round(event.rect.height);

        }).on('dragmove', function (event) {
            var data = c.data();
            var x = (data.x || 0) + event.dx;
            var y = (data.y || 0) + event.dy;
            event.target.setAttribute('data-x', x);
            event.target.setAttribute('data-y', y);
            c.data({
                x: x,
                y: y
            });
            c.css({
                transform: 'translateX(' + x + 'px) translateY(' + y + 'px) translateZ(0px)'
            });
        }).on('dragstop', function (event) {

        }).on('resizeend', function (event) {

        })
        $(popup).popup('show');
    }

    //Template layout change
    function templateChange(template_name) {
        var place = document.querySelector('#place'),
            template = document.querySelector('template#' + template_name),
            image = document.querySelector('.model');
        // Insert the model`s image inside the div
        template = document.importNode(template.content, true);
        template.querySelector('div').insertBefore(image, template.querySelector('div').firstChild);
        // If the place have some content, remove to append new layout
        while (place.hasChildNodes()) {
            place.removeChild(place.firstChild);
        }
        place.appendChild(template);
    }

    //crop image - canvas
    function cropCanvas(destination) {
        var crop_canvas,
            left = $('.crop-view').offset().left - $('#clone').offset().left,
            top = $('.crop-view').offset().top - $('#clone').offset().top,
            width = $('.crop-view').width(),
            height = $('.crop-view').height();

        crop_canvas = document.createElement('canvas');
        crop_canvas.width = width;
        crop_canvas.height = height;
        $('#clone').css({
            'margin-top': top,
            'margin-left': left
        });
        //        crop_canvas.getContext('2d').drawImage($('#clone')[0], 0, 0);
        var cropped = crop_canvas.getContext('2d').drawImage($('#clone')[0], left, top, width, height, 0, 0, width, height);
        //        console.log(destination);
        //        $(destination).append(cropped);

        window.open(crop_canvas.toDataURL("image/png"));
        $('#crop-popup').popup('hide');

    }

    // Função de remover item da shelve
    function removeElement(event) {

    }


    $(document).ready(function () {
        $('#crop-popup').popup({
            blur: false,
            onclose: function () {
                var popup = document.querySelector('#crop-popup');
                while (popup.hasChildNodes()) {
                    popup.removeChild(popup.firstChild);
                }
            }
        });
        templateChange('lay1');
    })